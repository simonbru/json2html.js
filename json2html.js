function json2html(obj) {
	var result = [];
	recursiveBuilder(obj);
	return result.join('');
	
	function append() {
		for (var i=0; i<arguments.length; i++) {
			result.push(arguments[i]);
		}
	}
	
	function recursiveBuilder(obj) {
		if (!(obj instanceof Object)){
			result.push(obj);
			return
		}
		if (obj.elem === undefined || obj.elem == "") {
			return;
		}
		
		// Tag
		append('<', obj.elem, ' ');
		
		// Attributes
		for (var i in obj) {
 
			if (i === 'style' && obj[i] instanceof Object) {
				var styles = obj[i];
				append(i, '="');
				for (var j in styles) {
					var escapedAttr = styles[j].replace(/"/g, "&quot;");
					append(j, ':', escapedAttr, ';');
				}
				append('"');
			}
			else if (i !== "elem" && i !== "elemContent") {
				// escape quotes in attributes
				var escapedAttr = obj[i].toString().replace(/"/g, "&quot;");

				if (i.indexOf('on') === 0 && obj[i] instanceof Function) {
					// A function (not defined as a String) is assigned as 
					// an inline event, we make it called in it
					append(i, '="(', escapedAttr, ')()" '); 
				} else {
					append(i, '="', escapedAttr, '" ');
				}
			}
		}
		
		if (obj.elemContent !== undefined) {
			append(">");
			// Content of the element
			if (obj.elemContent instanceof Array) {
				for (var i in obj.elemContent) {
					recursiveBuilder(obj.elemContent[i]);
				}
			} else {
				recursiveBuilder(obj.elemContent);
			}
			
			append('</', obj.elem, '>');
		} else {
			append('/>');
		}
	}
}

function json2dom(obj) {
	var result = recursiveBuilder(obj);
	return result;
	
	function recursiveBuilder(obj) {
		// obj isn't of type "Element"
		if (!(obj instanceof Object)){
			return document.createTextNode(obj);
		}
		
		// obj is a malformed node of type "Element"
		else if (obj.elem === undefined || obj.elem == "") {
			return document.createTextNode(obj);
		}
		
		// Node
		var node = document.createElement(obj.elem);
		
		// Attributes
		for (var i in obj) {
			// If a function (not defined as a String) is assigned as 
			// an inline event
			if (i.indexOf('on') === 0 && obj[i] instanceof Function) {
				node[i] = obj[i];
			}
			// If the styles are defined as an Object
			else if (i === 'style' && obj[i] instanceof Object) {
				var styles = obj[i];
				for (j in styles) {
					// If the 'js names' are used
					if (node.style[j] !== undefined) {
						console.log(styles);
						node.style[j] = styles[j];
					} else {
						// We insert the style as a string in the style attribute
						var escapedValue = styles[j].replace(/"/g, "&quot;");
						node.setAttribute('style', j + ":" + escapedValue + ";" + node.getAttribute('style'));
					}
				}
			}
			// Any other attribute
			else if (i !== "elem" && i !== "elemContent") {
				node.setAttribute(i, obj[i]);
			}
		}
		
		// Node childs
		if (obj.elemContent !== undefined) {
			if (obj.elemContent instanceof Array) {
				for (var i in obj.elemContent) {
					node.appendChild(recursiveBuilder(obj.elemContent[i]));
				}
			} else {
				node.appendChild(recursiveBuilder(obj.elemContent));
			}
		}	
		return node;
	}
}
